﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;
using UnityEngine.UI;
public class ControlNijaGirl : MonoBehaviour
{
    private const float walkSpeed = 5;
    private float velocity = 40f;
    private const float JumpForce = 5f;
    private const float doubleJumpForce = 8f;

    public AudioClip AudioJump;
    public AudioClip AudioAtaque;
    public AudioClip muerteNinja;
    public AudioClip AudioCoins;
    public ControlPuntaje Puntaje;
    public VidaText Vida;
    public ZombieDeadPoint zombieDeadPoint;
    
    private AudioSource _audioSource;
    private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer sr;
    private Transform _transform;
    public GameObject KunaiRigth;
    public GameObject KunaiLeft;
    private bool movimiento = true;
    
    private const int ANIM_QUIETO = 0;
    private const int ANIM_CORRER = 1;
    private const int ANIM_SALTAR = 2;
    private const int ANIM_ATACAR = 3;
    private const int ANIM_MUERTE = 4;
    private const int ANIM_AGACHA = 5;
    private const int ANIM_TREPAR = 6; //falta
    private const int ANIM_VOLAR = 7; //falta
    private const int ANIM_ATACAE = 8;
    private bool muerte = false;
    private int numSalto = 0;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        _transform = GetComponent<Transform>();
        _audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (movimiento)
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
            animator.SetInteger("Estado", ANIM_QUIETO);

            if (Input.GetKey(KeyCode.RightArrow))
            {
                rb.velocity = new Vector2(walkSpeed, rb.velocity.y);
                animator.SetInteger("Estado", ANIM_CORRER);
                sr.flipX = false;
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                rb.velocity = new Vector2(-walkSpeed, rb.velocity.y);
                animator.SetInteger("Estado", ANIM_CORRER);
                sr.flipX = true;
            }
            if (Input.GetKeyDown(KeyCode.UpArrow) && numSalto < 2)
            {
                if (numSalto == 0)
                    rb.AddForce(new Vector2(0, JumpForce), ForceMode2D.Impulse);
                else
                    rb.AddForce(new Vector2(0, doubleJumpForce), ForceMode2D.Impulse);
                animator.SetInteger("Estado", ANIM_SALTAR);
                _audioSource.PlayOneShot(AudioJump);
                numSalto++;
            }
            if (Input.GetKeyUp("x"))
            {
                animator.SetInteger("Estado", ANIM_ATACAR);
                if (!sr.flipX)
                {
                    var KunaiPosition = new Vector3(_transform.position.x + 3f, _transform.position.y, _transform.position.z);
                    Instantiate(KunaiRigth, KunaiPosition, Quaternion.identity);
                }
                if (sr.flipX)
                {
                    var KunaiPosition = new Vector3(_transform.position.x - 3f, _transform.position.y, _transform.position.z);
                    Instantiate(KunaiLeft, KunaiPosition, Quaternion.identity);
                }
                _audioSource.PlayOneShot(AudioAtaque);
            }

            if (Input.GetKeyDown("f"))
                animator.SetInteger("Estado", ANIM_ATACAE);

            if (Input.GetKey(KeyCode.DownArrow))
            {
                animator.SetInteger("Estado", ANIM_AGACHA);
            }

            if (muerte)
                animator.SetInteger("Estado", ANIM_MUERTE);
        }
    }
    
    private void OnBecameInvisible() //Slida de camara
    {
        transform.position = new Vector3(-6, -3, 0);

        if (Vida.GetVida() > 1)
            Vida.QuitarVida(1);
        else
            SceneManager.LoadScene("Menu");
    }
    
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Plataforma"))
        {
            transform.parent = collision.transform;
        }
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (Vida.GetVida() > 1)
                Vida.QuitarVida(1);
            else
            {
                SceneManager.LoadScene("Menu");
                _audioSource.PlayOneShot(muerteNinja);
            }
        }
        if (collision.gameObject.layer == 8)
        {
            numSalto = 0;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Plataforma"))
        {
            transform.parent = null;
        }
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Nivel"))
        {
            LoadNextLevel();
        }

        if (other.gameObject.CompareTag("Coins"))
        {
            Destroy(other.gameObject);
            _audioSource.PlayOneShot(AudioCoins);
            Puntaje.AddPoints(1);
        }
        
        if (other.gameObject.CompareTag("Enemy"))
        {
            if (Vida.GetVida() > 1)
                Vida.QuitarVida(1);
            else
                Vida.GetVida();
        }
    }
    public void LoadNextLevel()
    {
        if (Application.loadedLevel < Application.levelCount - 1)
            Application.LoadLevel(Application.loadedLevel + 1);
    }
    public void EnemyKnockback(float enemyPos)
    {
        float side = Mathf.Sign(enemyPos - transform.position.x);
        rb.AddForce(Vector2.left * side * JumpForce, ForceMode2D.Impulse);
        if (side > 0)
        {
            rb.AddForce(Vector2.up * side * JumpForce, ForceMode2D.Impulse);
        }
        else
        {
            rb.AddForce(Vector2.up * -side * JumpForce, ForceMode2D.Impulse);
        }
        animator.SetInteger("Estado", ANIM_SALTAR);
        movimiento = false;
        Invoke(nameof(Mover), 0.7f);
        Color color = new Color(255 / 255f, 106 / 255f, 0 / 255f);

        sr.color = color;

    }
    public void Mover()
    {
        movimiento = true;
        sr.color = Color.white;
    }
}
