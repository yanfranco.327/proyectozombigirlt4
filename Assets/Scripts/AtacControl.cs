﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtacControl : MonoBehaviour
{
    private ControlPuntaje Puntaje;
    void Start()
    {
        Puntaje = FindObjectOfType<ControlPuntaje>();
        Destroy(this.gameObject, 0.5f);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
            Puntaje.AddPoints(5);
        }
    }
}
