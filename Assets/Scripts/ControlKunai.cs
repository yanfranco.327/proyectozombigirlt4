﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlKunai : MonoBehaviour
{
    public float velocity;
    private Rigidbody2D rb;

    private ControlPuntaje Puntaje;
    private ZombieDeadPoint zombieDeadPoint;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        Puntaje = FindObjectOfType<ControlPuntaje>();
        zombieDeadPoint = FindObjectOfType<ZombieDeadPoint>();
        Destroy(this.gameObject, 3);
    }

    void Update()
    {
        rb.velocity = new Vector2(velocity, rb.velocity.y);
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 9)
        {
            Destroy(collision.gameObject);
            Destroy(this.gameObject);
            Puntaje.AddPoints(5);
            zombieDeadPoint.AddPointsZombieDead(1);
            Debug.Log(Puntaje.GetPoint());
            Debug.Log(zombieDeadPoint.GetZombieDead());
        }
    }
}
