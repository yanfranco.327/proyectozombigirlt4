﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtacMaker : MonoBehaviour
{
    public GameObject Cactus;
    private float tiempo = 0f;

    void Start()
    {
        Cactus.gameObject.SetActive(false);
    }

    void Update()
    {

        tiempo += Time.deltaTime;
        if (tiempo >= 2)
        {
            Cactus.gameObject.SetActive(true);
            if (tiempo >= 4)
            {
                Cactus.gameObject.SetActive(false);
                tiempo = 0;
            }
        }
    }
}
