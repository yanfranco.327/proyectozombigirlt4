using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZombieDeadPoint : MonoBehaviour
{
    private int puntajeZombie = 0;

    public Text PuntajeDeadZombie;
    public int GetZombieDead()
    {
        return puntajeZombie;
    }

    public void AddPointsZombieDead(int puntajeZombie)
    {
        this.puntajeZombie += puntajeZombie;
        PuntajeDeadZombie.text = "" + GetZombieDead();
    }
}
